import avatarImage from './avatar.png';
import carImage from './car.png'
import carBannerImage from './carBanner.png';
import BackgroundImage from './background.svg';

export {avatarImage, carImage, carBannerImage, BackgroundImage};