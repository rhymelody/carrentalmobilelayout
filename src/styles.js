import EStyleSheet from 'react-native-extended-stylesheet';

EStyleSheet.build({ 
  $bgColor: '#FFFFFF',
  $bgTone: '#D3D9FD',
  $darkBlue : {
    500: '#091B6F',
    400: '#0D28A6',
    300: '#5E70C4',
    200: '#AEB7E1',
    100: '#CFD4ED'
  },
  $limeGreen : {
    500: '#3D7B3F',
    400: '#5CB85F',
    300: '#92D094',
    200: '#C9E7CA',
    100: '#DEF1DF'
  },
  $Neutral : {
    500: '#151515',
    400: '#3C3C3C',
    300: '#8A8A8A',
    200: '#D0D0D0',
    100: '#FFFFFF'
  },
});

export default EStyleSheet;