export const CARLIST = [
    {
        carName : 'Daihatsu Xenia',
        carPassenger: 6,
        carLugage: 2,
        carPrice: 230000,
        recommendedCar: false
    },
    {
        carName : 'Honda Brio',
        carPassenger: 4,
        carLugage: 1,
        carPrice: 170000,
        recommendedCar: true
    },
    {
        carName : 'Toyota Avanza',
        carPassenger: 6,
        carLugage: 2,
        carPrice: 240000,
        recommendedCar: true
    },
    {
        carName : 'Toyota Fortuner',
        carPassenger: 8,
        carLugage: 2,
        carPrice: 325000,
        recommendedCar: true
    },
    {
        carName : 'Honda Mobilio',
        carPassenger: 6,
        carLugage: 2,
        carPrice: 265000,
        recommendedCar: false
    },
    {
        carName : 'Suzuki Ertiga',
        carPassenger: 6,
        carLugage: 2,
        carPrice: 215000,
        recommendedCar: false
    },
    {
        carName : 'Nissan Grand Livina',
        carPassenger: 6,
        carLugage: 2,
        carPrice: 305000,
        recommendedCar: false
    },
    {
        carName : 'Suzuki APV Arena',
        carPassenger: 8,
        carLugage: 2,
        carPrice: 255000,
        recommendedCar: true
    },
    {
        carName : 'Toyota Yaris',
        carPassenger: 4,
        carLugage: 2,
        carPrice: 285000,
        recommendedCar: false
    },
    {
        carName : 'Mitsubishi Xpander',
        carPassenger: 8,
        carLugage: 2,
        carPrice: 335000,
        recommendedCar: true
    }
];