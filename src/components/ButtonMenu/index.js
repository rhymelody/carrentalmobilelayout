import React from 'react';
import { TouchableOpacity, Text, View, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

import styles from './styles';

const ButtonMenu = ({icon, caption}) => {
  return (
    <View style={styles.container}>
        <TouchableOpacity onPress={() => alert('Coming soon!')} activeOpacity={0.5} style={styles.button}>
            <Icon style={styles.icon} name={icon}/>
        </TouchableOpacity>
        <Text style={styles.caption}>{caption}</Text>
    </View>
  )
};

export default ButtonMenu;
