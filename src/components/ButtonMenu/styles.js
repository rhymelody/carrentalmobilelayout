import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    container: {
        alignItems: 'center',
        paddingHorizontal: '.25rem'
    },
    button: {
        borderRadius: '.5rem',
        backgroundColor: '$limeGreen[100]',
        marginBottom: '.5rem',
        padding: '1rem',
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        fontSize: '2.25rem',
        color: '$limeGreen[400]'
    },
    caption: {
        color: '$Neutral[500]',
        fontSize: '.75rem'
    }
});

export default styles;