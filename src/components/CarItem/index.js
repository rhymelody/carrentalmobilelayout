import React from 'react';
import { TouchableOpacity, View, Text, Image } from 'react-native';
import NumberFormat from 'react-number-format';
import Icon from 'react-native-vector-icons/Feather';

import { carImage } from '../../assets';
import styles from './styles.js';

const CarItem = ({name, passanger, lugage, price}) => {
   
    return (
        <View >
            <TouchableOpacity onPress={() => alert('Coming soon!')} activeOpacity={0.5} style={styles.container}>
                <Image style={styles.image} source={carImage} resizeMode="contain"/>
                <View style={styles.item}>
                    <Text style={styles.item.name}>{name}</Text>
                    <View style={styles.item.description}>
                        <View style={styles.item.description.container}>
                            <Icon name="users" style={styles.item.description.icon}/>
                            <Text style={styles.item.description.caption}>{passanger}</Text>
                        </View>
                        <View style={styles.item.description.container}>
                            <Icon name="briefcase" style={styles.item.description.icon}/>
                            <Text style={styles.item.description.caption}>{lugage}</Text>
                        </View>
                    </View>
                    <NumberFormat 
                        prefix="Rp "
                        thousandSeparator="."
                        decimalSeparator="," 
                        value={price} 
                        displayType="text" 
                        renderText={
                            value => <Text style={styles.item.price}>{value}</Text>
                    }/>
                </View>
            </TouchableOpacity>
        </View>
    );
};

export default CarItem;