import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    container: {
        borderRadius: '.5rem',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '$bgColor',
        shadowColor: '$Neutral[500]',
        shadowOffset: { width: 1, height: 0 },
        shadowOpacity: 0.15,
        shadowRadius: 4,  
        elevation: 4,
        marginBottom: '1rem'
    },
    image: {
        margin: '1rem',
        width: '4.5rem',
        height:'4.5rem',
        flexGrow: 0.125
    },
    item: {
        flexGrow: 25,
        paddingVertical: '1rem',
        justifyContent: 'center',
        flex: 1,
        name: {
            color: '$Neutral[500]',
            fontSize: '1rem',
            marginBottom: '.275rem'
        },
        description: {
            flexDirection: 'row',
            marginBottom: '.75rem',
            container: {
                flexDirection: 'row',
                alignItems: 'center',
                marginRight: '.75rem'
            },
            icon: {
                color: '$Neutral[300]',
                fontSize: '.875rem',
                marginRight: '.25rem'
            },
            caption: {
                color: '$Neutral[300]',
                fontSize: '.75rem',
            }
        },
        price : {
            color: '$limeGreen[400]',
            fontSize: '.975rem'
        }
    },
    
});

export default styles;
