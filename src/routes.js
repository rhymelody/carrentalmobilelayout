import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Feather';

import EStyleSheet from './styles';
import { Home, ListCar, Account } from './pages';

const Tab = createBottomTabNavigator();

const Routes = () => {
  const insets = useSafeAreaInsets();

  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={{
          headerShown: false,
          tabBarActiveTintColor: EStyleSheet.value("$darkBlue['400']"),
          tabBarInactiveTintColor: EStyleSheet.value("$Neutral['500']"),
          tabBarStyle:{
            height: insets.bottom + 58,
            paddingTop: 8,
            paddingBottom: insets.bottom || 8
          }
      }}>
        <Tab.Screen 
          name="Home"
          component={Home}
          options={{
            tabBarIcon: ({ color, size }) => (
              <Icon name="home" size={size} color={color} />
            )
        }}/>
        <Tab.Screen 
          name="Daftar Mobil" 
          component={ListCar}
          options={{
            tabBarIcon: ({ color, size }) => (
              <Icon name="list" size={size} color={color} />
            )
        }}/>
        <Tab.Screen 
          name="Akun" 
          component={Account}
          options={{
            tabBarIcon: ({ color, size }) => (
              <Icon name="user" size={size} color={color} />
            )
        }}/>
      </Tab.Navigator>
    </NavigationContainer>
  )
}

export default Routes;