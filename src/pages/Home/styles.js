import EStyleSheet from 'react-native-extended-stylesheet';
import {Dimensions} from 'react-native';

const screen = Dimensions.get("screen");

const styles = EStyleSheet.create({
    $screenHeight: screen.height,
    $screenWidth: screen.width,
    safeArea: {
        backgroundColor: '$bgTone', 
        paddingTop: '2rem',
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: '1.5rem',
        paddingVertical: '1rem',
        backgroundColor: '$bgTone',
        name: {
            color: '$Neutral[500]',
            marginBottom: '.25rem',
            fontSize: '1rem'
        },
        location: {
            color: '$Neutral[500]',
            fontWeight: 'bold',
            fontSize: '1.125rem'
        }
    },
    container: {
        backgroundColor: '$bgColor'
    },
    tone:{
        position: 'absolute', 
        top: -screen.height, 
        left: 0, right: 0,
        backgroundColor: '$bgTone',  
        height: '$screenHeight + 5 rem'
    },
    content: {
        paddingHorizontal: '1.25rem',
    },
    avatar: {
        height: '2rem',
        width: '2rem',
        borderRadius: '2rem / 2'
    },
    banner: {
        overflow: 'hidden',
        backgroundColor: '$darkBlue[500]',
        borderRadius: '.5rem',
        width: '100%',
        height: screen.height / 5.5,
        marginBottom: '1.25rem',
        postion: 'relative',
        content:{
            flex: 1,
            padding: '1.5rem',
            justifyContent: 'center',
            alignItems: 'flex-start',
            zIndex: 100,
            elevation: 100,
            width: '100%',
            height: '100%',
            position: 'absolute',
            caption: {
                color: '$Neutral[100]',
                fontSize: 16,
                lineHeight: '1.5rem',
                marginBottom: '1.125rem'
            },
            button: {
                borderRadius: '.25 rem',
                backgroundColor: '$limeGreen[400]',
                paddingVertical: '.5rem',
                paddingHorizontal: '1.125rem',
                caption: {
                    color: '$Neutral[100]',
                    fontSize: 12,
                    fontWeight: 'bold',
                    flexDirection: 'row', justifyContent: 'flex-start'
                }
            }
        },
        background: {
            width: '54.5%',
            height: '100%',
            position: 'absolute',
            bottom: 0,
            right: 0,
            pallete: {
                backgroundColor: '$darkBlue[400]',
                borderColor: '$darkBlue[400]',
                borderWidth: 5,
                borderTopLeftRadius: '4.25rem',
                width: '100%',
                height: '4.25rem',
                position: 'absolute',
                bottom: 0,
                right: 0,
            },
            image: {
                position: 'absolute',
                bottom: 0,
                right: 0,
                width: '100%',
                height: '72.5%'
            }
        }
    },
    menu: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: '1.75rem'
    },
    recommend: {
        header: {
            color: '$Neutral[500]',
            fontWeight: 'bold',
            fontSize: '1.125rem',
            marginBottom: '1rem'
        }
    }
});

export default styles;