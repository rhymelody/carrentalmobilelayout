import React from 'react';
import { Text, View, SafeAreaView, Image, ScrollView, TouchableOpacity } from 'react-native'
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import { ButtonMenu, CarItem } from '../../components/';
import { avatarImage, carBannerImage } from '../../assets';
import { CARLIST } from '../../data';
import styles from './styles';

const Home = () =>{
  const tabBarHeight = useBottomTabBarHeight();

  return (
        <SafeAreaView style={styles.safeArea}>
          <View style={styles.header}>
            <View>
              <Text style={styles.header.name}>Hi, Ilham Santosa</Text>
              <Text style={styles.header.location}>East Java</Text>
            </View>
            <TouchableOpacity style={{justifyContent:'center'}}>
              <Image style={styles.avatar} resizeMode="cover" source={avatarImage} />
            </TouchableOpacity>
          </View>
          <ScrollView style={styles.container} contentContainerStyle={{paddingBottom: tabBarHeight + (tabBarHeight / 2)}}>
            <View style={styles.content}>
              <View style={styles.tone}/>
              <View style={styles.banner}>
                  <View style={styles.banner.content}>
                    <Text style={styles.banner.content.caption}>Sewa Mobil Berkualitas{'\n'}di kawasanmu</Text>
                    <TouchableOpacity 
                      onPress={() => alert('Coming soon!')}
                      style={styles.banner.content.button}
                    >
                      <Text style={styles.banner.content.button.caption}>Sewa Mobil</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.banner.background}>
                    <View style={styles.banner.background.pallete}/>
                    <Image style={styles.banner.background.image} resizeMode="contain" source={carBannerImage}/>
                  </View>
              </View>
              <View style={styles.menu}>
                <ButtonMenu icon="truck" caption="Sewa Mobil"/>
                <ButtonMenu icon="box" caption="Oleh Oleh"/>
                <ButtonMenu icon="key" caption="Penginapan"/>
                <ButtonMenu icon="camera" caption="Wisata"/>
              </View>
              <View style={styles.recommend}>
                <Text style={styles.recommend.header}>Daftar Mobil Pilihan</Text>
                <View style={styles.recommend.list}>
                  {CARLIST.map((data, index) => data.recommendedCar && <CarItem key={index}
                    name={data.carName} 
                    passanger={data.carPassenger} 
                    lugage={data.carLugage} 
                    price={data.carPrice}/>)}
                </View>
              </View>          
            </View>
          </ScrollView>
        </SafeAreaView>
  );
};

export default Home;
