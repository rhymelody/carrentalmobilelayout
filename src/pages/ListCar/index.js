import React from 'react';
import { Text, View, SafeAreaView, ScrollView } from 'react-native';
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';

import { CarItem } from '../../components/';
import { CARLIST } from '../../data';
import styles from './styles';

const ListCar = () => {
  const tabBarHeight = useBottomTabBarHeight();

  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.header}>
        <Text style={styles.header.caption}>Daftar Mobil</Text>
      </View>
      <ScrollView contentContainerStyle={{paddingBottom: tabBarHeight + (tabBarHeight / 16)}}>
        <View style={styles.content}>
          {CARLIST.map((data, index) => <CarItem key={index}
              name={data.carName} 
              passanger={data.carPassenger} 
              lugage={data.carLugage} 
              price={data.carPrice}/>)}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default ListCar;