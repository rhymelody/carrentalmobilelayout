import EStyleSheet from 'react-native-extended-stylesheet';
import {Dimensions} from 'react-native';

const screen = Dimensions.get("screen");

const styles = EStyleSheet.create({
    safeArea: {
        backgroundColor: '$bgColor', 
        paddingTop: '2rem',
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '$bgColor',
        paddingHorizontal: '1.5rem',
        paddingVertical: '1rem',
        caption: {
            color: '$Neutral[500]',
            fontWeight: 'bold',
            fontSize: '1.125rem'
        }
    },
    content: {
        padding: '1.25rem',
        paddingBottom: 0
    }
});

export default styles;