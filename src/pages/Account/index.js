import React from 'react';
import { Text, View, TouchableOpacity, SafeAreaView, Dimensions, ScrollView } from 'react-native';
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';

import { BackgroundImage } from '../../assets';
import styles from './styles';

const Account = () => {
  const screen = Dimensions.get("screen");
  const tabBarHeight = useBottomTabBarHeight();

  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.header}>
        <Text style={styles.header.caption}>Akun</Text>
      </View>
      <ScrollView contentContainerStyle={[styles.content, {paddingBottom: tabBarHeight}]}>
        <View style={styles.information}>
          <BackgroundImage width={screen.width}/>
          <Text style={styles.information.caption}>Upss kamu belum memiliki akun. Mulai buat akun agar transaksi di BCR lebih mudah!</Text>
          <TouchableOpacity 
            onPress={() => alert('Coming soon!')}
            style={styles.information.button}
          >
            <Text style={styles.information.button.caption}>Register</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Account;