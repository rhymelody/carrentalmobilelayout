import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    safeArea: {
        backgroundColor: '$bgColor', 
        paddingTop: '2rem',
        flex: 1
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '$bgColor',
        paddingHorizontal: '1.5rem',
        paddingVertical: '1rem',
        caption: {
            color: '$Neutral[500]',
            fontWeight: 'bold',
            fontSize: '1.125rem'
        }
    },
    content: {
        padding: '1.25rem',
        alignItems: 'center', justifyContent: 'center',
        flex: 1
    },
    information: {
        padding: '1rem',
        alignItems: 'center', justifyContent: 'center',
        flex: 1,
        image: {
            flexGrow: 1
        },
        caption: {
            paddingVertical: '1rem',
            color: '$Neutral[500]',
            textAlign: 'center',
            fontSize: '1rem'
        },
        button: {
            borderRadius: '.5rem',
            backgroundColor: '$limeGreen[400]',
            paddingVertical: '.675rem',
            paddingHorizontal: '2rem',
            caption: {
                color: '$Neutral[100]',
                fontSize: '1rem'
            }
        }
    }
});

export default styles;